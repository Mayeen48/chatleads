<?php

namespace App\Http\Controllers;

use App\film;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class WelcomeController extends Controller
{
    public function index(Request $request, $page = 1)
    {
        
        $client = new Client();
        try {
            $call = $client->get('http://localhost/chatleads/api/welcome_data?page=' . $page);
            $response = json_decode($call->getBody()->getContents(), true);
        } catch (\Exception $e) {
            return "No data found";
            //buy a beer
        }
        // return $response;
        $datas = $response['data'];
        $numOfpages = $response['last_page'];
        $current_page = $response['current_page'];
        $current_page_url = $response['first_page_url'];
        $has_next_page = empty($response['next_page_url'])?0:1;
        $has_previous_page = empty($response['prev_page_url'])?0:1;
        $prev_page_url = $response['prev_page_url'];
        $next_page = $response['next_page_url'];

        $from = $response['from'];
        $to = $response['to'];
        $last_page_url = $response['last_page_url'];
        $per_page = $response['per_page'];
        $total_page = $response['total'];
        
        return view(
            'frontend.pages.welcome', compact(
                'datas','numOfpages', 'current_page','current_page_url','from','to','last_page_url','per_page','total_page',
                'prev_page_url','has_next_page', 'has_previous_page', 'next_page',
            )
        );
        
    }
}
