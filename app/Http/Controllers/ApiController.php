<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Http;
use App\film;
use App\comment;

class ApiController extends Controller
{
    public function apiFilmData()
    {
        $all_films = film::paginate(3);
        return response()->json($all_films);
    }
    public function filmDetails($film_id){
        $film_details= film::where('film_id',$film_id)->first();

        $comment_details= comment::select('comments.*','users.name')
        ->join('users','comments.user_id','=','users.id')
        ->where('comments.film_id',$film_id)->get();
        $film_details['comments']=$comment_details;
        return response()->json($film_details);
    }

    public function filmCreate(Request $request){
        $film_update_id=$request->film_update_id;
        $film_name=$request->film_name;
        $description=$request->description;
        $release=$request->release;
        $date=$request->date;
        $rating=$request->rating;
        $ticket=$request->ticket;
        $price=$request->price;
        $country=$request->country;
        $genre=$request->genre;
        $file_name='';
        if ($release==null) {
            $release=0;
        }
        if ($request->hasFile('photo')) {
            $file = $request->file('photo');
            $file_name = time() . $file->getClientOriginalName();
            $destinationPath = public_path('backend/images/film_images/');
            $file->move($destinationPath, $file_name);
        }
        $film_array=array(
            'film_name'=>$film_name,
            'description'=>$description,
            'release'=>$release,
            'date'=>$date,
            'rating'=>$rating,
            'ticket'=>$ticket,
            'price'=>$price,
            'country'=>$country,
            'genre'=>$genre,
            'photo'=>$file_name,
        );
        if ($film_update_id!=null) {
            # code...
        }else{
            film::insert($film_array);
        }
        return \response()->json(['message'=>"Insert Success", 'class_name'=>'alert-success']);
    }
    public function commentCreate(Request $request){
        $film_id=$request->film_id;
        $user_id=$request->user_id;
        $comment=$request->comment;
        $comment_array=array(
            'film_id'=>$film_id,
            'user_id'=>$user_id,
            'comments'=>$comment
        );
        comment::insert($comment_array);
        return \response()->json(['message'=>"Success",'class_name'=>"alert-success"]);
    }
}
