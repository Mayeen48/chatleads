<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use App\film;
use App\comment;

class FilmController extends Controller
{
    public function filmDetails($film_id)
    {
        // return $film_id;
        // $req_data=$request->all();
        $client = new Client();
        try {
            $call = $client->get('http://localhost/chatleads/api/film_details/' . $film_id);
            $film_details = json_decode($call->getBody()->getContents(), true);
        } catch (\Exception $e) {
            return "No data found";
            //buy a beer
        }
        // $film_details = $client->request("get", "http://localhost/chatleads/api/film_details/".$film_id);

        // $film_details= film::where('film_id',$film_id)->first();

        // $comment_details= comment::select('comments.*','users.name')
        // ->join('users','comments.user_id','=','users.id')
        // ->where('comments.film_id',$film_id)->get();
        // $film_details['comments']=$comment_details;
        // return $film_details;

        return view('frontend.films.film_details',compact('film_details'));
    }
    public function filmForm()
    {
        return view('frontend.films.film_form');
    }
    public function filmCreate(Request $request){
        // return $request->all();
        $req_data=$request->all();
        $client = new Client();
        return $res = $client->request("post", "http://localhost/chatleads/api/film_create", $req_data);
    }
    public function commentCreate(Request $request){
        $film_id=$request->film_id;
        $user_id=$request->user_id;
        $comment=$request->comment;
        $comment_array=array(
            'film_id'=>$film_id,
            'user_id'=>$user_id,
            'comments'=>$comment
        );
        comment::insert($comment_array);
        return \response()->json(['message'=>"Success",'class_name'=>"alert-success"]);
    }
}
