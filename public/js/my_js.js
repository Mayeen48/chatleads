$(document).ready(function() {
    $('#description').keypress(function(event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            event.preventDefault();
            var film_id = $('#film_id').val();
            var comment = $('#description').val();
            var user_id = $('#user_id').val();
            var user_name = $('.user_loged').html();
            var data = { film_id: film_id, user_id: user_id, comment: comment }
            if (user_id) {
                var comment_API = requestUrl("http://localhost/chatleads/api/comment_create", data);
                comment_API.then(res => res.json())
                    .then(
                        json => {
                            // console.log(json);
                            var html = '';
                            html += '<p style="margin-left: 20px;">'
                            html += '<font color="blue">' + user_name + '</font> ' + comment
                            html += '</p>'
                            $('.comment_show').append(html);
                        })
            } else {
                alert_message('#alert_message_show', 'alert-danger', "Please login first")
                    // console.log("Please login first");
            }
            $('#description').val('');
        }
    });

    $('#film_form_submit').on('submit', function(e) {
        e.preventDefault();
        // alert("Clicked");
        var film_name = $('#film_name').val();
        var description = $('#description').val();
        var release = $("input[name='release']:checked").val();
        var date = $('#date').val();
        var rating = $("input[name='ratings']:checked").val();
        var ticket = $('#ticket').val();
        var price = $('#price').val();
        var country = $('#country').val();
        var genre = $('#genre').val();
        var photo = $('#photo').val();
        // var file_size = $("#photo")[0];
        // console.log(rating);
        // return 0;
        var photo_file = $("#photo")[0].files[0];
        if (film_name == '') {
            alert_message('#alert_message_show', 'alert-danger', "Film name required")
            scrollToDiv('#alert_message_show');
            return 0;
        } else if (description == '') {
            alert_message('#alert_message_show', 'alert-danger', "Description is required")
            scrollToDiv('#alert_message_show');
            return 0;
        } else if (release == null) {
            alert_message('#alert_message_show', 'alert-danger', "Release is required")
            scrollToDiv('#alert_message_show');
            return 0;
        } else if (date == '') {
            alert_message('#alert_message_show', 'alert-danger', "Date is required")
            scrollToDiv('#alert_message_show');
            return 0;
        } else if (rating == '') {
            alert_message('#alert_message_show', 'alert-danger', "Rating is required")
            scrollToDiv('#alert_message_show');
            return 0;
        } else if (ticket == '') {
            alert_message('#alert_message_show', 'alert-danger', "Ticket is required")
            scrollToDiv('#alert_message_show');
            return 0;
        } else if (price == '') {
            alert_message('#alert_message_show', 'alert-danger', "Price is required")
            scrollToDiv('#alert_message_show');
            return 0;
        } else if (country == '') {
            alert_message('#alert_message_show', 'alert-danger', "Country is required")
            scrollToDiv('#alert_message_show');
            return 0;
        } else if (genre == '') {
            alert_message('#alert_message_show', 'alert-danger', "Genre is required")
            scrollToDiv('#alert_message_show');
            return 0;
        } else if (photo == '') {
            alert_message('#alert_message_show', 'alert-danger', "Photo is required")
            scrollToDiv('#alert_message_show');
            return 0;
        }
        // test 
        var form = new FormData();
        // const stream = fs.createReadStream(service3.check_folder_path + '/' + checked_files[i]);
        form.append('film_name', film_name);
        form.append('description', description);
        form.append('release', release);
        form.append('date', date);
        form.append('rating', rating);
        form.append('ticket', ticket);
        form.append('price', price);
        form.append('country', country);
        form.append('genre', genre);
        form.append('photo', photo_file);
        const film_save_request = new Request("http://localhost/chatleads/api/film_create", {
            method: 'POST',
            body: form
        });
        fetch(film_save_request)
            .then(response => response.json())
            .then(data => {
                console.log(data);
                alert_message('#alert_message_show', data.class_name, data.message)
                scrollToDiv('#alert_message_show');
                $('#film_form_submit')[0].reset();
            }).catch(function() {
                console.log("May be internet problem");
            });
    })
    $('#user_reg').on('click', function(e) {
        e.preventDefault();
        $('#new_user_modal').modal('show');
        // alert("Clicked");
    })

    //User Create
    $('#new_user_save').on('click', function(event) {
            event.preventDefault();
            var message_id = '#user_message';
            // alert("Hi");
            // return false;
            var name = $("#name").val();
            var email = $("#email").val();
            var password = $("#password").val();
            var error_class = 'alert-danger';
            var success_class = 'alert-success';
            //  console.log(permissions);
            //  return false;
            var password_confirm = $("#password-confirm").val();
            if (name == "" || email == "" || password == "" || password_confirm == "") {
                alert_message(message_id, error_class, Globals.all_fields_required);
                return false;
            }
            if (password != password_confirm) {
                alert_message(message_id, error_class, Globals.password_not_match);
                return false;
            }
            var user_data = { name: name, email: email, password: password };
            var user_API = requestUrl("http://localhost/chatleads/api/user_create", user_data);
            user_API.then(res => res.json())
                .then(
                    json => {
                        var response = json;
                        console.log(response);
                        var message_id = '#user_message';
                        if (response.message == 'success') {
                            // $("#div").load(" #div > *");
                            $('#new_user_modal').modal('hide');
                            alert_message('#alert_message_show', success_class, Globals.user_created);
                        } else if (response.message == 'invalid') {
                            alert_message(message_id, error_class, Globals.email_already_database);
                        } else if (response.message == 'name_required') {
                            alert_message(message_id, error_class, Globals.name_length);
                        } else if (response.message == 'email_required') {
                            alert_message(message_id, error_class, Globals.email_length);
                        } else if (response.message == 'pass_required') {
                            alert_message(message_id, error_class, Globals.password_length);
                        }
                    });
            // $.ajax({
            //     headers: {
            //         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            //     },
            //     url: Globals.base_url + 'user_create',
            //     type: 'POST',
            //     dataType: 'JSON',
            //     data: { name: name, email: email, password: password },
            //     success: function(response) {
            //         console.log(response);
            //         var message_id = '#user_message';
            //         if (response.message == 'success') {
            //             // $("#div").load(" #div > *");
            //             $('#new_user_modal').modal('hide');
            //             alert_message('#alert_message_show', success_class, Globals.user_created);
            //         } else if (response.message == 'invalid') {
            //             alert_message(message_id, error_class, Globals.email_already_database);
            //         } else if (response.message == 'name_required') {
            //             alert_message(message_id, error_class, Globals.name_length);
            //         } else if (response.message == 'email_required') {
            //             alert_message(message_id, error_class, Globals.email_length);
            //         } else if (response.message == 'pass_required') {
            //             alert_message(message_id, error_class, Globals.password_length);
            //         }
            //     }
            // })
        })
        //User Create End

})