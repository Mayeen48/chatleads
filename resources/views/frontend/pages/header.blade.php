<div class="row">
    <div class="col-md-10" style="height: 50px;">
        {{-- <div class="row">
            <div class="col-md-2">
                <div class="logo">
                    <img src="{{asset('public/dashboard/logo/frontend_logo.jpg')}}" class="img-responsive jacos_logo">
                </div>
            </div>
            <div class="col-md-10">
                <div class="logo_menu">
                    <button type="button" class="btn btn-primary">受信ボックス</button>
                    <button type="button" class="btn btn-secondary">データ管理</button>
                    <button type="button" class="btn btn-success">ユーザ管理</button>
                </div>
            </div>
        </div> --}}
    </div>
    <div class="col-md-2 pull-right">
        <ul class="uname text-right">
            @if (Route::has('login'))
            @auth
            <li class="dropdown"><a href="#" class="user_loged" class="dropdown-toggle">{{Auth::user()->name}}</a>
                <ul class="dropdown-menu">
                    <li>
                        <a class="dropdown-item text-danger" href="<?php echo(\Config::get('app.url').'logout');?>"
                            onclick="event.preventDefault();
                                            document.getElementById('logout-form').submit();">
                            <i class="material-icons text-danger">&#xE879;</i>
                            Log out
                        </a>
                        <form id="logout-form" action="<?php echo(\Config::get('app.url').'logout');?>" method="POST"
                            style="display: none;">
                            @csrf
                        </form>
                    </li>
                </ul>
            </li>
            {{-- <a href="{{ url('/home') }}">Home</a> --}}
            @else
            <li class="dropdown"><a href="{{ route('login') }}" class="user_loged">Login</a></li>
            {{-- <a href="{{ route('login') }}">Login</a> --}}

            @if (Route::has('register'))
            <li class="dropdown"><a href="{{ route('register') }}" class="user_loged" id="user_reg">Register</a></li>
            {{-- <a href="{{ route('register') }}"></a> --}}
            @endif
            @endauth
            @endif
        </ul>
    </div>
</div>