@extends('frontend.layouts.master')
@section('content')
<div class="row">
  <div class="col-md-12" id="alert_message_show">
    @include('backend.flash_message.flash_message')
  </div>
  <div class="col-md-12">
    <div class="middle_cl_area cmn_cl_border">
      <div class="text-left">
        <a href="{{Config::get('app.url').'add_film/'}}" type="button" class="btn btn-info insert_nw">Add Film</a>
      </div>
      <br>
      <br>
      <div class="clearfix"></div>

      <div class="clearfix"></div>
      <div class="row">
        @foreach ($datas as $film)
        <div class="col-md-4">
          <a href="{{Config::get('app.url').'film_details/'.$film['film_id']}}">
        <img
          src="{{empty($film['photo'])||$film['photo']==0?Config::get('app.url').'public/backend/images/no_image/no_image.jpg':Config::get('app.url').'public/backend/images/film_images/'. $film['photo']}}"
          class="img-fluid imgsection" alt="{{ $film['photo'] }}">
        </a>
      </div>
      @endforeach
        <div class="col-md-12">
        @if(isset($current_page))
        <?php
        $prev = $current_page - 1;
      ?>
        @if(($has_next_page == true) && ($has_previous_page == false))
        <li><a href="{{Config::get('app.url').'home/'.($current_page+1)}}">Next</a></li>
        @elseif(($has_next_page == false) && ($has_previous_page == true))
        <li><a href="{{Config::get('app.url').'home/'.$prev}}">Previous</a></li>
        @elseif(($has_next_page == true) && ($has_previous_page == true))
        <li><a href="{{Config::get('app.url').'home/'.$prev}}">Previous</a></li>
        <li><a href="{{Config::get('app.url').'home/'.($current_page+1)}}">Next</a></li>
        @endif
        @endif
      </div>
         {{-- @if(isset($current_page)) --}}
        <?php
        // $prev = $current_page - 1;
      ?>
        {{-- @if(($has_next_page == true) && ($has_previous_page == false))
         <li><a href="{{url('home/'.$next_page)}}">prev</a></li> 
        <li><a href="{{$next_page}}">Next</a></li>
        @elseif(($has_next_page == false) && ($has_previous_page == true))
        <li><a href="{{$prev_page_url}}">Previous</a></li>
        @elseif(($has_next_page == true) && ($has_previous_page == true))
        <li><a href="{{$prev_page_url}}">Previous</a></li>
        <li><a href="{{$next_page}}">Next</a></li>
        @endif
        @endif  --}}
        {{-- {{print_r(json_decode($all_films))}} --}}
        {{-- @foreach ($all_films as $film)
        <div class="col-md-4">
          <a href="{{Config::get('app.url').'film_details/'.$film->film_id}}">
        <img
          src="{{empty($film->photo)||$film->photo==0?Config::get('app.url').'public/backend/images/no_image/no_image.jpg':Config::get('app.url').'public/backend/images/film_images/'. $film->photo}}"
          class="img-fluid imgsection" alt="{{ $film->photo }}">
        </a>
      </div>
      @endforeach --}}
      <div class="col-md-12">
        {{-- {{print_r($all_films)}} --}}
        {{-- {{ $all_films->links() }} --}}
      </div>
    </div>
  </div>
</div>
</div>
@endsection