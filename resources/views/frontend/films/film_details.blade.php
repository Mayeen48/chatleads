@extends('frontend.layouts.master')
@section('content')
<div class="row">
    <div class="col-md-12" id="alert_message_show">
        @include('backend.flash_message.flash_message')
    </div>
    <div class="col-md-12">
        <div class="middle_cl_area cmn_cl_border">
            <div class="text-left">
                {{-- <button type="button" class="btn btn-lg btn-outline-dark"><span class="cat_main">.....</span>社</button>
        <button type="button" class="btn btn-lg btn-outline-dark">送信元番号</button> --}}
                <a href="{{Config::get('app.url').'home/'}}" type="button" class="btn btn-info insert_nw">Home</a>
                <a href="{{Config::get('app.url').'add_film/'}}" type="button" class="btn btn-info insert_nw">Add
                    Film</a>
                {{-- <button type="button" class="btn btn-info insert_nw">Add Film</button> --}}
            </div>

            <br>
            <div class="clearfix"></div>

            <div class="clearfix"></div>
            <input type="hidden" id="film_id" name="film_id" value="{{ $film_details['film_id'] }}">
            <input type="hidden" id="user_id" name="user_id" value="@Auth{{Auth::user()->id}}@endAuth">
            <div class="row">
                <div class="col-md-4">
                    <img src="{{empty($film_details['photo'])||$film_details['photo']==0?Config::get('app.url').'public/backend/images/no_image/no_image.jpg':Config::get('app.url').'public/backend/images/film_images/'. $film_details['photo']}}" class="img-fluid imgsection"
                            alt="{{ $film_details['photo'] }}">
                </div>
                <div class="col-md-4">
                    <p>Name: {{$film_details['film_name']}}</p>
                    <p>Description: {{$film_details['description']}}</p>
                    <p>Release: {{$film_details['release']==0?"No":"Yes"}}</p>
                    <p>Date: {{$film_details['date']}}</p>
                    <p>Rating: {{$film_details['rating']}}</p>
                    <p>Ticket: {{$film_details['ticket']}}</p>
                </div>
                <div class="col-md-4">
                    <p>Price: {{$film_details['price']}}</p>
                    <p>Country: {{$film_details['country']}}</p>
                    <p>Genre: {{$film_details['genre']}}</p>
                </div>
            </div>
            
        </div>
    </div>
    <div class="col-md-4"></div>
    <div class="col-md-6 comment">
        <div class="comment_show">
            <h3>Comments</h3>
            @if (!empty($film_details['comments']))
                @foreach ($film_details['comments'] as $comment)
                <p style="margin-left: 20px;">
                    <font color="blue">{{$comment['name']}}</font> {{$comment['comments']}}
                </p>
                @endforeach
            @else
                
            @endif
            
        </div>
        @Auth
        <div class="form-group row">
            {{-- <label for="description" class="col-4 col-form-label">Description</label>  --}}
            <div class="col-8">
                <textarea id="description" name="description" cols="40" rows="2" class="form-control" placeholder="Comment"></textarea>
            </div>
        </div>
        @endAuth
    </div>
    <div class="col-md-2"></div>
</div>
@endsection