@extends('frontend.layouts.master')
@section('content')
<div class="row">
  <div class="col-md-12" id="alert_message_show">
    @include('backend.flash_message.flash_message')
  </div>
  <div class="col-md-3"></div>
  <div class="col-md-6">
    <div class="middle_cl_area cmn_cl_border">
        <div class="text-left">
            {{-- <button type="button" class="btn btn-lg btn-outline-dark"><span class="cat_main">.....</span>社</button>
            <button type="button" class="btn btn-lg btn-outline-dark">送信元番号</button> --}}
            <a href="{{Config::get('app.url').'home/'}}" type="button" class="btn btn-info insert_nw">Home</a>
            <a href="{{Config::get('app.url').'add_film/'}}" type="button" class="btn btn-info insert_nw">Add Film</a>
            {{-- <button type="button" class="btn btn-info insert_nw">Add Film</button> --}}
          </div>
    
          <br>
          <div class="clearfix"></div>
    
          <div class="clearfix"></div>
        <form id="film_form_submit" action="{{Config::get('app.url').'add_film'}}" method="POST" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="film_update_id" value="">
            <div class="form-group row">
              <label for="film_name" class="col-4 col-form-label">Name</label> 
              <div class="col-8">
                <input id="film_name" name="film_name" type="text" class="form-control">
              </div>
            </div>
            <div class="form-group row">
              <label for="description" class="col-4 col-form-label">Description</label> 
              <div class="col-8">
                <textarea id="description" name="description" cols="40" rows="5" class="form-control"></textarea>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-4">Release</label> 
              <div class="col-8">
                <div class="custom-control custom-radio custom-control-inline">
                  <input name="release" id="radio_0" type="radio" class="custom-control-input" value="1"> 
                  <label for="radio_0" class="custom-control-label">Yes</label>
                </div>
                <div class="custom-control custom-radio custom-control-inline">
                  <input name="release" id="radio_1" type="radio" class="custom-control-input" value="0"> 
                  <label for="radio_1" class="custom-control-label">NO</label>
                </div>
              </div>
            </div>
            <div class="form-group row">
              <label for="date" class="col-4 col-form-label">Date</label> 
              <div class="col-8">
                <input id="date" name="date" type="date" class="form-control">
              </div>
            </div>
            <input type="hidden" id="rating_val" value="">
            <div class="form-group row">
              <label for="rating" class="col-4 col-form-label">Rating</label> 
              <div class="col-8 rating">
                <label>
                  <input type="radio" name="ratings" value="1" />
                  <span class="icon">★</span>
                </label>
                <label>
                  <input type="radio" name="ratings" value="2" />
                  <span class="icon">★</span>
                  <span class="icon">★</span>
                </label>
                <label>
                  <input type="radio" name="ratings" value="3" />
                  <span class="icon">★</span>
                  <span class="icon">★</span>
                  <span class="icon">★</span>   
                </label>
                <label>
                  <input type="radio" name="ratings" value="4" />
                  <span class="icon">★</span>
                  <span class="icon">★</span>
                  <span class="icon">★</span>
                  <span class="icon">★</span>
                </label>
                <label>
                  <input type="radio" name="ratings" value="5" />
                  <span class="icon">★</span>
                  <span class="icon">★</span>
                  <span class="icon">★</span>
                  <span class="icon">★</span>
                  <span class="icon">★</span>
                </label>
              </div>
            </div>
            <div class="form-group row">
              <label for="ticket" class="col-4 col-form-label">Ticket</label> 
              <div class="col-8">
                <input id="ticket" name="ticket" type="text" class="form-control">
              </div>
            </div>
            <div class="form-group row">
              <label for="price" class="col-4 col-form-label">Price</label> 
              <div class="col-8">
                <input id="price" name="price" type="text" class="form-control">
              </div>
            </div>
            <div class="form-group row">
              <label for="country" class="col-4 col-form-label">Country</label> 
              <div class="col-8">
                <select id="country" name="country" class="custom-select">
                  <option value="Austrailia">Austrailia</option>
                  <option value="America">America</option>
                  <option value="Bangladesh" selected>Bangladesh</option>
                  <option value="Canada">Canada</option>
                  <option value="China">China</option>
                  <option value="Dutch">Dutch</option>
                  <option value="England">England</option>
                  <option value="Finland">Finland</option>
                  <option value="Japan">Japan</option>
                  <option value="Korea">Korea</option>
                </select>
              </div>
            </div>
            <div class="form-group row">
              <label for="genre" class="col-4 col-form-label">Genre</label> 
              <div class="col-8">
                <select id="genre" name="genre" class="custom-select">
                  <option value="Action">Action</option>
                  <option value="Adventure">Adventure</option>
                  <option value="Comedy">Comedy</option>
                  <option value="Crime & Gangster">Crime & Gangster</option>
                  <option value="Drama">Drama</option>
                  <option value="Epic & Historical">Epic & Historical</option>
                  <option value="Horror">Horror</option>
                  <option value="Musical & Dance">Musical & Dance</option>
                  <option value="Science Fiction">Science Fiction</option>
                  <option value="War">War</option>
                  <option value="Western">Western</option>
                </select>
              </div>
            </div>
            <div class="form-group row">
              <label for="photo" class="col-4 col-form-label">Photo</label> 
              <div class="col-8">
                <input id="photo" name="photo" class="custom-select input-file" type="file">
                {{-- <select id="select2" name="select2" class="custom-select">
                  <option value="rabbit">Rabbit</option>
                  <option value="duck">Duck</option>
                  <option value="fish">Fish</option>
                </select> --}}
              </div>
            </div> 
            <div class="form-group row">
              <div class="offset-4 col-8">
                <button name="submit" type="submit" class="btn btn-primary">Submit</button>
              </div>
            </div>
          </form>
    </div>
  </div>
  <div class="col-md-3"></div>
</div>
@endsection