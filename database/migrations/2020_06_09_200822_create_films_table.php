<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFilmsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('films', function (Blueprint $table) {
            $table->increments('film_id')->comment('film_id');
            $table->string('film_name',100)->nullable()->comment('film_name');
			$table->text('description')->comment('description')->nullable();
            $table->boolean('release')->default(0)->comment('release');
            $table->date('date')->comment('date')->nullable();
            $table->string('rating',10)->comment('rating')->nullable();
            $table->string('ticket', 40)->comment('ticket')->nullable();
            $table->string('price', 20)->comment('price')->nullable();
            $table->string('country', 50)->comment('country')->nullable();
            $table->string('genre', 40)->comment('genre')->nullable();
            $table->string('photo', 240)->comment('Image of Film')->nullable();
			$table->dateTime('created_at')->default(DB::raw('CURRENT_TIMESTAMP'))->comment('Time of creation');
			$table->dateTime('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'))->comment('Time of Update');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('films');
    }
}
