<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\film;
use Faker\Generator as Faker;
use Illuminate\Support\Facades\Storage;
$factory->define(film::class, function (Faker $faker) {
    return [
        'film_name' => $faker->sentence(5),
        'description' => $faker->realText(rand(80, 600)),
        'release' => rand(0, 1),
        'date' => $faker->date(),
        'rating' => rand(1, 5),
        'ticket' => rand(100, 500),
        'price' => rand(1000, 5000),
        'country' => $faker->country(),
        'genre' => $faker->word,
        'photo' => $faker->image('public/backend/images/film_images', 400, 300, null, false),

    ];
});
