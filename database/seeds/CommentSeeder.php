<?php

use Illuminate\Database\Seeder;
use App\comment;

class CommentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $comment_array=array(
            [
                'film_id' => 1,
                'user_id' => 1,
                'comments' => "Nice movie"
            ],
            [
                'film_id' => 2,
                'user_id' => 2,
                'comments' => "Nice movie"
            ],
            [
                'film_id' => 3,
                'user_id' => 3,
                'comments' => "Nice movie"
            ],
            [
                'film_id' => 4,
                'user_id' => 1,
                'comments' => "Nice movie"
            ],
            [
                'film_id' => 5,
                'user_id' => 2,
                'comments' => "Nice movie"
            ],
            [
                'film_id' => 6,
                'user_id' => 3,
                'comments' => "Nice movie"
            ],
        );
        comment::insert($comment_array);
    }
}
