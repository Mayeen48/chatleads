<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('welcome_data', 'ApiController@apiFilmData');
Route::get('film_details/{film_id}', 'ApiController@filmDetails');
Route::post('film_create', 'ApiController@filmCreate');
Route::post('comment_create', 'ApiController@commentCreate');
Route::post('user_create', 'UserManagement@userCreate');
