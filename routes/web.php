<?php
use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('language/{locale}', function ($locale) {
    Session::put('locale',$locale);

    return redirect()->back();
});
// Route::get('/', function () {
// 	return view('frontend.welcome');
// 	// return redirect('login');
// });
Route::get('/', function(){
	return redirect('/home');
});

Auth::routes();

// Route::get('/home/', 'WelcomeController@index')->name('home');
Route::get('/home/{page?}', 'WelcomeController@index');


// Authentication Route

Route::get('film_details/{film_id}', 'FilmController@filmDetails');
Route::get('add_film/{id?}', 'FilmController@filmForm');
Route::post('add_film', 'FilmController@filmCreate');


